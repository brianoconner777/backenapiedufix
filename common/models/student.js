var Promise = require('bluebird');
var path = require('path');
var ObjectId = require('mongodb').ObjectID;

var config = require('../../server/config.json');

module.exports = function(Student) {
  // Remove email validation
  // delete Student.validations.email;

  Student.disableRemoteMethod("create", true);
  // Student.disableRemoteMethod("find", true);
  // Student.disableRemoteMethod("findById", true);
  // Student.disableRemoteMethod("update", false);
  // Student.disableRemoteMethod("updateById", false);
  // Student.disableRemoteMethod("exists", true);
  Student.disableRemoteMethod("upsert", true);
  Student.disableRemoteMethod("count", true);
  Student.disableRemoteMethod("delete", true);
  Student.disableRemoteMethod("deleteById", true);
  Student.disableRemoteMethod("updateAll", true);
  Student.disableRemoteMethod("createChangeStream", true);
  Student.disableRemoteMethod("findOne", true);
  // Student.disableRemoteMethod("updateAttributes", false);
  Student.disableRemoteMethod("confirm", false);

  Student.disableRemoteMethod('__get__class', true);

  Student.disableRemoteMethod('__count__accessTokens', false);
  Student.disableRemoteMethod('__create__accessTokens', false);
  Student.disableRemoteMethod('__delete__accessTokens', false);
  Student.disableRemoteMethod('__destroyById__accessTokens', false);
  Student.disableRemoteMethod('__findById__accessTokens', false);
  Student.disableRemoteMethod('__get__accessTokens', false);
  Student.disableRemoteMethod('__updateById__accessTokens', false);

  Student.disableRemoteMethod('__updateById__skill', false);

  Student.disableRemoteMethod('__count__reviews', false);
  // Student.disableRemoteMethod('__create__reviews', false);
  Student.disableRemoteMethod('__delete__reviews', false);
  // Student.disableRemoteMethod('__destroyById__reviews', false);
  Student.disableRemoteMethod('__findById__reviews', false);
  // Student.disableRemoteMethod('__get__reviews', false);
  Student.disableRemoteMethod('__updateById__reviews', false);

  /**
   * Utility Methods
   */
  function throwError(msg, errCode) {
    var toSend = new Error(msg);
    toSend.statusCode = errCode;
    return toSend
  }

  function checkUpdateAttributes(body) {
    return new Promise(function(resolve, reject) {
      if (body.hasOwnProperty("initForm")) {
        if (body.initForm) {
          // Check for more attributes
          if (body.hasOwnProperty("email")) {
            resolve(true);
          } else {
            reject(throwError("'email' is missing", 400));
          }
        } else {
          resolve(true);
        }
      } else {
        reject(throwError("'initForm' is missing", 400));
      }
    });
  }

  function checkStudentModel(id) {
    return new Promise(function(resolve, reject) {
      Student.findById(id, function(err, d) {
        if (d) resolve(d.username);
        else reject(true);
      })
    });
  }


  /**
   * Before Remotes
   */

  /**
   * Checks Email and initform exist in
   * Update request
   */
  Student.beforeRemote('*.updateAttributes', function(ctx, instance, next) {
    checkUpdateAttributes(ctx.req.body)
      .then(function(response) {
        next();
      })
      .catch(function(err) {
        next(err);
      });
  });

  Student.beforeRemote('*.__create__reviews', function(ctx, instance, next) {
    if (ctx.req.headers.authorization)
      var at = ctx.req.headers.authorization;
    else if (ctx.req.query.access_token)
      var at = ctx.req.query.access_token;

    if (!at) next(throwError("Cannot Find access_token", 400))
    else Student.app.models.AccessToken.findById(at, function(err, user) {
      if (err) next(throwError(err, 500));
      else
        checkStudentModel(user.userId)
        .then(function(rId) {
          if (rId == ctx.req.params.id)
            next(throwError("Not Authorized", 400))
          else {
            ctx.req.body.reviewerId = user.userId;
            ctx.req.body.reviewerType = "student";
            next();
          }
        })
        .catch(function() {
          ctx.req.body.reviewerId = user.userId;
          ctx.req.body.reviewerType = "teacher";
          next();
        });
    })
  });

  /**
   * Checks weather a skill exist Already
   */
  Student.beforeRemote('*.__create__skill', function(ctx, inst, next) {
    var Skill = Student.app.models.skill;
    var AccessToken = Student.app.models.AccessToken;

    if (ctx.req.headers.authorization)
      var at = ctx.req.headers.authorization;
    else if (ctx.req.query.access_token)
      var at = ctx.req.query.access_token;

    if (!at) next(throwError("Cannot Find access_token", 400))
    else AccessToken.findById(at, function(err, user) {
      if (!at) next(throwError("Cannot Find access_token", 400));
      else {
        Skill.findOne({
          where: {
            teacherId: user.userId,
            skillName: ctx.req.body.skillName
          }
        }, function(err, d) {
          if (d) next(throwError("Skill Already Exist", 206));
          else next();
        });
      }
    });
  });

  /**
   * After Remotes
   */

  /**
   * Login After Remote to show the status of
   * InitForm
   */
  Student.afterRemote('login', function(ctx, result, next) {
    if (!result.hasOwnProperty("error")) {
      Student.findById(result.userId, function(err, res) {
        if (err) next(throwError("Cannot find id in database", 500));
        else {
          result.initForm = res.initForm;
          next();
        }
      });
    } else next();
  });

  /**
   * Update email before initform
   * If initForm: 0 then update email and verify
   */
  Student.updateEmail = function(id, email, cb) {
    Student.update({
      _id: id
    }, {
      '$set': {
        'email': email,
        'initForm': 1,
        'emailVerified': false
      }
    }, function(err) {
      if (err) cb(err, null);
      else {
        Student.findById(id, function(err, userInstance) {
          var options = {
            type: 'email',
            to: email,
            from: 'noreply@loopback.com',
            subject: 'Thanks for registering.',
            template: path.resolve(__dirname, '../../server/views/verify.ejs'),
            redirect: "http://" + config.host + "/student",
            user: userInstance
          };

          userInstance.verify(options, function(err, response, next) {
            if (err) {
              cb(err, null);
              return;
            }

            console.log('> verification email sent:', response);
            cb(null, {
              status: 200,
              message: "Email Verification Sent"
            });
          });
        });
      }
    });
  };

  Student.remoteMethod('updateEmail', {
    description: "Update email before initform",
    http: {
      path: '/:id/updateEmail',
      verb: 'post'
    },
    accepts: [{
      arg: 'id',
      type: 'string',
      description: 'studentID',
      required: true
    }, {
      arg: 'email',
      type: 'string',
      description: 'New Email ID',
      required: true
    }],
    returns: {
      arg: 'response',
      type: 'Object'
    }
  });



  /**
   * Endorse student with skill
   */
  function endorseCounter(sid, id, type) {
    var Skills = Student.app.models.skill;
    if (type == 'push')
      Skills.update({
        id: new ObjectId(sid)
      }, {
        '$push': {
          'recommends': id
        }
      });
    else
      Skills.update({
        id: new ObjectId(sid)
      }, {
        '$pull': {
          'recommends': id
        }
      });
  }

  Student.endorse = function(id, sid, cb) {
    var Skills = Student.app.models.skill;

    Skills.findById(sid, function(err, msg) {

      if (err) cb(err, null);
      else if (!msg) cb(throwError("Skill Does not Exist", 422));
      else if (msg.studentId == id)
        cb(throwError("Not Authorized", 401));
      else {
        var eLen = msg.recommends.length;
        if (msg.recommends.indexOf(id) === -1) {
          endorseCounter(sid, id, 'push');
          eLen++;
        } else {
          endorseCounter(sid, id, 'pull');
          eLen--;
        }

        cb(null, {
          msg: "Endorsed Successfully",
          eLen: eLen
        });
      }
    });
  };

  Student.remoteMethod('endorse', {
    description: "Endorse student with skill",
    http: {
      path: '/:id/endorse/:sid',
      verb: 'get'
    },
    accepts: [{
      arg: 'id',
      type: 'string',
      description: "Endorser's ID"
    }, {
      arg: 'sid',
      type: 'string',
      description: "Skill instance's ID"
    }],
    returns: {
      arg: 'response',
      type: 'object'
    }
  });


  /**
   * Send Email before reseting password
   */
  Student.resetPasswordConfirm = function(access_token, password, cb) {
    Student.app.models.AccessToken.findById(access_token, function(err, user) {
      if (err || !user) cb(throwError("Cannot find access token", 500), null);
      else
        Student.findById(user.userId, function(err, user) {
          user.updateAttribute('password', password, function(err, user) {
            if (err) cb(err, null)
            else cb(null, user);
          });
        });
    });
  };

  Student.remoteMethod('resetPasswordConfirm', {
    description: "Change password with access token",
    http: {
      path: '/resetPasswordConfirm',
      verb: 'post'
    },
    accepts: [{
      arg: 'access_token',
      type: 'string',
      description: "Access Token",
      required: true
    }, {
      arg: 'password',
      type: 'string',
      description: "New password",
      required: true
    }],
    returns: {
      arg: 'response',
      type: 'object'
    }
  })


  Student.on('resetPasswordRequest', function(info) {
    var url = 'http://' + config.host + '/student/resetPassword';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password';

    Student.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });

};
