var Promise = require('bluebird');
var path = require('path');
var ObjectId = require('mongodb').ObjectID;

var config = require('../../server/config.json');

module.exports = function(Teacher) {
  // Remove email validation
  // delete Teacher.validations.email;

  Teacher.disableRemoteMethod("create", true);
  // Teacher.disableRemoteMethod("find", true);
  // Teacher.disableRemoteMethod("update", true);
  // Teacher.disableRemoteMethod("findById", true);
  // Teacher.disableRemoteMethod("exists", true);
  Teacher.disableRemoteMethod("upsert", true);
  Teacher.disableRemoteMethod("count", true);
  Teacher.disableRemoteMethod("delete", true);
  Teacher.disableRemoteMethod("deleteById", true);
  Teacher.disableRemoteMethod("updateAll", true);
  Teacher.disableRemoteMethod("createChangeStream", true);
  Teacher.disableRemoteMethod("findOne", true);
  // Teacher.disableRemoteMethod("updateAttributes", true);
  Teacher.disableRemoteMethod("confirm", false);

  Teacher.disableRemoteMethod('__count__classes', false);
  Teacher.disableRemoteMethod('__create__classes', true);
  Teacher.disableRemoteMethod('__delete__classes', true);
  Teacher.disableRemoteMethod('__destroyById__classes', true);
  Teacher.disableRemoteMethod('__findById__classes', false);
  Teacher.disableRemoteMethod('__get__classes', true);
  Teacher.disableRemoteMethod('__updateById__classes', false);


  Teacher.disableRemoteMethod('__count__accessTokens', false);
  Teacher.disableRemoteMethod('__create__accessTokens', false);
  Teacher.disableRemoteMethod('__delete__accessTokens', false);
  Teacher.disableRemoteMethod('__destroyById__accessTokens', false);
  Teacher.disableRemoteMethod('__findById__accessTokens', false);
  Teacher.disableRemoteMethod('__get__accessTokens', false);
  Teacher.disableRemoteMethod('__updateById__accessTokens', false);

  Teacher.disableRemoteMethod('__updateById__skill', false);

  Teacher.disableRemoteMethod('__count__reviews', false);
  // Teacher.disableRemoteMethod('__create__reviews', false);
  Teacher.disableRemoteMethod('__delete__reviews', false);
  // Teacher.disableRemoteMethod('__destroyById__reviews', false);
  Teacher.disableRemoteMethod('__findById__reviews', false);
  // Teacher.disableRemoteMethod('__get__reviews', false);
  Teacher.disableRemoteMethod('__updateById__reviews', false);

  /**
   * Utility Methods
   */
  function throwError(msg, errCode) {
    var toSend = new Error(msg);
    toSend.statusCode = errCode;
    return toSend
  }

  function checkTeacherModel(id) {
    return new Promise(function(resolve, reject) {
      Teacher.findById(id, function(err, d) {
        if (d) resolve(d.username);
        else reject(true);
      })
    });
  }

  /**
   * Before Remotes
   */

  /**
   * Checks weather a skill exist Already
   */
  Teacher.beforeRemote('*.__create__skill', function(ctx, inst, next) {
    var Skill = Teacher.app.models.skill;
    var AccessToken = Teacher.app.models.AccessToken;

    if (ctx.req.headers.authorization)
      var at = ctx.req.headers.authorization;
    else if (ctx.req.query.access_token)
      var at = ctx.req.query.access_token;

    if (!at) next(throwError("Cannot Find access_token", 400))
    else AccessToken.findById(at, function(err, user) {
      if (!at) next(throwError("Cannot Find access_token", 400));
      else {
        Skill.findOne({
          where: {
            teacherId: user.userId,
            skillName: ctx.req.body.skillName
          }
        }, function(err, d) {
          if (d) next(throwError("Skill Already Exist", 206));
          else next();
        });
      }
    });
  });

  Teacher.beforeRemote('*.__create__reviews', function(ctx, instance, next) {
    if (ctx.req.headers.authorization)
      var at = ctx.req.headers.authorization;
    else if (ctx.req.query.access_token)
      var at = ctx.req.query.access_token;

    if (!at) next(throwError("Cannot Find access_token", 400))
    else Teacher.app.models.AccessToken.findById(at, function(err, user) {
      if (err) next(throwError(err, 500));
      else
        checkTeacherModel(user.userId)
        .then(function(rId) {
          if (rId == ctx.req.params.id)
            next(throwError("Not Authorized", 400))
          else {
            ctx.req.body.reviewerId = user.userId;
            ctx.req.body.reviewerType = "teacher";
            next();
          }
        })
        .catch(function() {
          ctx.req.body.reviewerId = user.userId;
          ctx.req.body.reviewerType = "student";
          next();
        });
    })
  });

  /**
   * After Remotes
   */

  /**
   * Login After Remote to show the status of
   * InitForm
   */
  Teacher.afterRemote('login', function(ctx, result, next) {
    if (!result.hasOwnProperty("error")) {
      Teacher.findById(result.userId, function(err, res) {
        if (err) next(throwError("Cannot find id in database", 500));
        else {
          result.initForm = res.initForm;
          next();
        }
      });
    } else next();
  });

  /**
   * Update email before initform
   * If initForm: 0 then update email and verify
   */
  Teacher.updateEmail = function(id, email, cb) {
    Teacher.update({
      _id: id
    }, {
      '$set': {
        'email': email,
        'initForm': 1,
        'emailVerified': false

      }
    }, function(err) {
      if (err) cb(err, null);
      else {
        Teacher.findById(id, function(err, userInstance) {
          var options = {
            type: 'email',
            to: email,
            from: 'noreply@loopback.com',
            subject: 'Thanks for registering.',
            template: path.resolve(__dirname, '../../server/views/verify.ejs'),
            redirect: "http://" + config.host + "/teacher",
            user: userInstance
          };

          userInstance.verify(options, function(err, response, next) {
            if (err) {
              cb(err, null);
              return;
            }

            console.log('> verification email sent:', response);
            cb(null, {
              status: 200,
              message: "Email Verification Sent"
            });
          });
        });
      }
    });
  };

  Teacher.remoteMethod('updateEmail', {
    description: "Update email before initform",
    http: {
      path: '/:id/updateEmail',
      verb: 'post'
    },
    accepts: [{
      arg: 'id',
      type: 'string',
      description: 'teacherID',
      required: true
    }, {
      arg: 'email',
      type: 'string',
      description: 'New Email ID',
      required: true
    }],
    returns: {
      arg: 'response',
      type: 'Object'
    }
  });


  /**
   * Endorse teacher with skill
   */

  function endorseCounter(sid, id, type) {
    var Skills = Teacher.app.models.skill;
    if (type == 'push')
      Skills.update({
        id: new ObjectId(sid)
      }, {
        '$push': {
          'recommends': id
        }
      });
    else
      Skills.update({
        id: new ObjectId(sid)
      }, {
        '$pull': {
          'recommends': id
        }
      });
  }

  Teacher.endorse = function(id, sid, cb) {
    var Skills = Teacher.app.models.skill;

    Skills.findById(sid, function(err, msg) {

      if (err) cb(err, null);
      else if (!msg) cb(throwError("Skill Does not Exist", 422));
      else if (msg.teacherId == id)
        cb(throwError("Not Authorised", 401));
      else {
        var eLen = msg.recommends.length;
        if (msg.recommends.indexOf(id) === -1) {
          endorseCounter(sid, id, 'push');
          eLen++;
        } else {
          endorseCounter(sid, id, 'pull');
          eLen--;
        }

        cb(null, {
          msg: "Endorsed Successfully",
          eLen: eLen
        });
      }
    });
  };

  Teacher.remoteMethod('endorse', {
    description: "Endorse teacher with skill",
    http: {
      path: '/:id/endorse/:sid',
      verb: 'get'
    },
    accepts: [{
      arg: 'id',
      type: 'string',
      description: "Endorser's ID"
    }, {
      arg: 'sid',
      type: 'string',
      description: "Skill instance's ID"
    }],
    returns: {
      arg: 'response',
      type: 'object'
    }
  });


  Teacher.resetPasswordConfirm = function(access_token, password, cb) {
    Teacher.app.models.AccessToken.findById(access_token, function(err, user) {
      if (err || !user) cb(throwError("Cannot find access token", 500), null);
      else
        Teacher.findById(user.userId, function(err, user) {
          user.updateAttribute('password', password, function(err, user) {
            if (err) cb(err, null)
            else cb(null, user);
          });
        });
    });
  };

  Teacher.remoteMethod('resetPasswordConfirm', {
    description: "Change password with access token",
    http: {
      path: '/resetPasswordConfirm',
      verb: 'post'
    },
    accepts: [{
      arg: 'access_token',
      type: 'string',
      description: "Access Token",
      required: true
    }, {
      arg: 'password',
      type: 'string',
      description: "New password",
      required: true
    }],
    returns: {
      arg: 'response',
      type: 'object'
    }
  })


  Teacher.on('resetPasswordRequest', function(info) {
    var url = 'http://' + config.host + '/teacher/resetPassword';
    var html = 'Click <a href="' + url + '?access_token=' +
      info.accessToken.id + '">here</a> to reset your password';

    Teacher.app.models.Email.send({
      to: info.email,
      from: info.email,
      subject: 'Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });

};
