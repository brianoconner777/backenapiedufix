module.exports = function(Competition) {
  // Competition.disableRemoteMethod("create", false);
  Competition.disableRemoteMethod("find", true);
  // Competition.disableRemoteMethod("findById", true);
  Competition.disableRemoteMethod("update", true);
  Competition.disableRemoteMethod("exists", true);
  Competition.disableRemoteMethod("upsert", true);
  Competition.disableRemoteMethod("count", true);
  Competition.disableRemoteMethod("delete", true);
  Competition.disableRemoteMethod("deleteById", true);
  Competition.disableRemoteMethod("updateAll", true);
  Competition.disableRemoteMethod("createChangeStream", true);
  Competition.disableRemoteMethod("findOne", true);
  // Competition.disableRemoteMethod("updateAttributes", false);
};
