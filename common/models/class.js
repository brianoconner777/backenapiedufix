module.exports = function(Class) {
  Class.disableRemoteMethod("create", false);
  Class.disableRemoteMethod("find", false);
  Class.disableRemoteMethod("update", false);
  Class.disableRemoteMethod("findById", false);
  Class.disableRemoteMethod("exists", false);
  Class.disableRemoteMethod("upsert", false);
  Class.disableRemoteMethod("count", false);
  Class.disableRemoteMethod("delete", false);
  Class.disableRemoteMethod("deleteById", false);
  Class.disableRemoteMethod("updateAll", false);
  Class.disableRemoteMethod("createChangeStream", false);
  Class.disableRemoteMethod("findOne", false);
  Class.disableRemoteMethod("updateAttributes", false);
  Class.disableRemoteMethod("confirm", false);

  Class.disableRemoteMethod('__count__students', false);
  Class.disableRemoteMethod('__create__students', false);
  Class.disableRemoteMethod('__delete__students', false);
  Class.disableRemoteMethod('__destroyById__students', false);
  Class.disableRemoteMethod('__findById__students', false);
  Class.disableRemoteMethod('__get__students', false);
  Class.disableRemoteMethod('__updateById__students', false);
};
