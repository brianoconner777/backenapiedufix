module.exports = function(Representative) {
  Representative.disableRemoteMethod("create", false);
  Representative.disableRemoteMethod("find", true);
  // Representative.disableRemoteMethod("findById", true);
  Representative.disableRemoteMethod("update", true);
  Representative.disableRemoteMethod("exists", true);
  Representative.disableRemoteMethod("upsert", true);
  Representative.disableRemoteMethod("count", true);
  Representative.disableRemoteMethod("delete", true);
  Representative.disableRemoteMethod("deleteById", true);
  Representative.disableRemoteMethod("updateAll", true);
  Representative.disableRemoteMethod("createChangeStream", true);
  Representative.disableRemoteMethod("findOne", true);
  // Representative.disableRemoteMethod("updateAttributes", false);

  /**
   * Create Representative
   * {"name": "newsRep", "id": "newsRep"}
   * Create Roles
   *
   * {
      "principalType": "USER",
      "principalId": "test",
      "schoolId": "DPShwr",
      "type": "student"
    }
    *
    * NEWS Representative: `newsRep`
    * AWARDS Representative: `awardsRep`
    * COMPETITION Representative: `compRep`
   */
}
