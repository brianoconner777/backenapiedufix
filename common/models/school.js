var path = require('path');

/**
 * NOTE: During student creation put emailVerified: true
 */

module.exports = function(School) {

  School.disableRemoteMethod("create", false);
  School.disableRemoteMethod("find", true);
  // School.disableRemoteMethod("update", true);
  // School.disableRemoteMethod("findById", false);
  School.disableRemoteMethod("exists", true);
  School.disableRemoteMethod("upsert", true);
  School.disableRemoteMethod("count", true);
  School.disableRemoteMethod("delete", true);
  School.disableRemoteMethod("deleteById", true);
  School.disableRemoteMethod("updateAll", true);
  School.disableRemoteMethod("createChangeStream", true);
  School.disableRemoteMethod("findOne", true);
  // School.disableRemoteMethod("updateAttributes", false);
  School.disableRemoteMethod("confirm", true);

  School.disableRemoteMethod('__count__students', false);
  School.disableRemoteMethod('__create__students', true);
  School.disableRemoteMethod('__delete__students', true);
  School.disableRemoteMethod('__destroyById__students', true);
  School.disableRemoteMethod('__findById__students', false);
  School.disableRemoteMethod('__get__students', false);
  School.disableRemoteMethod('__updateById__students', false);
  // School.disableRemoteMethod('__updateBy__students', false);

  School.disableRemoteMethod('__count__teachers', false);
  School.disableRemoteMethod('__create__teachers', true);
  School.disableRemoteMethod('__delete__teachers', true);
  School.disableRemoteMethod('__destroyById__teachers', true);
  School.disableRemoteMethod('__findById__teachers', false);
  School.disableRemoteMethod('__get__teachers', false);
  School.disableRemoteMethod('__updateById__teachers', false);

  School.disableRemoteMethod('__delete__news', false);
  School.disableRemoteMethod('__count__news', false);

  School.disableRemoteMethod('__count__accessTokens', false);
  School.disableRemoteMethod('__create__accessTokens', false);
  School.disableRemoteMethod('__delete__accessTokens', false);
  School.disableRemoteMethod('__destroyById__accessTokens', false);
  School.disableRemoteMethod('__findById__accessTokens', false);
  School.disableRemoteMethod('__get__accessTokens', false);
  School.disableRemoteMethod('__updateById__accessTokens', false);
};
