var Promise = require('bluebird');

var MongoClient = require("mongodb").MongoClient;
var mongoConf = require("../../server/datasources.json").edufix;

if(mongoConf.user && mongoConf.password)
  var uri = "mongodb://"+mongoConf.user+":"+mongoConf.password+"@"+mongoConf.host+":"+mongoConf.port+"/"+mongoConf.database;
else var uri = "mongodb://"+mongoConf.host+":"+mongoConf.port+"/"+mongoConf.database;

module.exports = function(Skill) {
  Skill.disableRemoteMethod("create", true);
  Skill.disableRemoteMethod("find", true);
  Skill.disableRemoteMethod("update", true);
  Skill.disableRemoteMethod("findById", true);
  Skill.disableRemoteMethod("exists", true);
  Skill.disableRemoteMethod("upsert", true);
  Skill.disableRemoteMethod("count", true);
  Skill.disableRemoteMethod("delete", true);
  Skill.disableRemoteMethod("deleteById", true);
  Skill.disableRemoteMethod("updateAll", true);
  Skill.disableRemoteMethod("replaceById", true);
  Skill.disableRemoteMethod("replaceOrCreate", true);
  Skill.disableRemoteMethod("upsertWithWhere", true);
  Skill.disableRemoteMethod("createChangeStream", true);
  Skill.disableRemoteMethod("findOne", true);
  Skill.disableRemoteMethod("updateAttributes", true);
  Skill.disableRemoteMethod("confirm", true);

  // Talent Search with only skills
  // Talent Search with skills and name
  // Talent Search with skills, name and schoolId
  function profile(at, type, cb) {
    return new Promise(function(resolve, reject) {
      Skill.app.models.AccessToken.findById(at, function(err, user){
        if(type == "student") {
          Skill.app.models.Student.findById(user.userId, function(err, user){
            if(err) reject(err);
            else resolve(user);
          });
        }else if(type == "teacher") {
          Skill.app.models.Teacher.findById(user.userId, function(err, user){
            if(err) reject(err);
            else resolve(user);
          });
        } else reject("Invalid Account Type");
      });
    });
  }

  function search(schoolId, query, db) {
    return new Promise(function(resolve, reject){
      db.collection('skill').aggregate([
        { $match: { skillName: new RegExp(query, 'i')} },
        { $lookup: {
            from: "student", localField: "studentId", foreignField: "_id", as: "students"
          }
        },
        {
          $project: {
            "student": {
                    $filter: { input: "$students", as: "student", cond: { $eq: [ "$$student.schoolId", schoolId ] } }
                },
            skillName: true
          }
        }
      ], function(err, res) {
        if(err) reject(err);
        else resolve(res);
      });
    });
  }

  Skill.talentSearch = function(q, type, at, cb) {
    MongoClient.connect(uri, function(err, db) {
      profile(at, type)
      .then(function(user){
        search(user.schoolId, q, db)
        .then(function(res) {
          cb(null, res);
        })
        .catch(function(err){
          cb(err, null);
        })
      })
      .catch(function(err){
        cb(err, null);
      });
    });
  };

  Skill.remoteMethod('talentSearch', {
    description: "Search student with their skills",
    http: {
      path: '/talentSearch',
      verb: 'post'
    },
    accepts: [{
      arg: 'query',
      type: 'string',
      description: "Skill Name",
      required: true
    },{
      arg: 'type',
      type: 'string',
      description: "Account Type",
      required: true
    }, {
      arg: 'access_token',
      type: 'string',
      description: "Access Token"
    }],
    returns: {
      arg: 'response',
      type: 'object'
    }
  });
};
